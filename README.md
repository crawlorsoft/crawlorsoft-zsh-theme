# crawlorsoft-zsh

At CrawlorSoft, we use the command line.  A lot.  To this end we've developed this oh-my-zsh theme to give us the information we find pertinent during our work so that we don't waste time trying to decipher things we don't necessarily need to know.

This theme contains the following customizations:

1. Left Hand Side:
    1. (OPT) If the last command failed (red skull).
    2. (OPT) The number of background processes currently running.
    3. The current file path.
2. Right Hand Side - If the current file path is a VCS repository, the pertinent information is displayed WHEN it makes sense.  This includes:
    1. VCS type
    2. VCS branch (if the prompt is red, then you are working in the main branch and NOT a topic / feature branch)
    3. (OPT) The current action state of the vcs, if applicable.
    3. (OPT) The number of commits behind the remote branch the local branch is.
    4. (OPT) The number of commits ahead of the remote branch the local branch is.
    5. (OPT) The number of files being added to the repo.
    6. (OPT) The number of files modified from the repo.
    7. (OPT) The number of files deleted from the repo.
    8. (OPT) The number of files renamed.
    9. (OPT) The number of files which are not tracked against the repo.

