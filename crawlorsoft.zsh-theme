BG_PROCESS_ICON='\uf955'
FAILED_PROCESS_ICON='\ufb8a'
PROMPT_END='❱'
BRANCH_INDICATOR=$'\ue725'
PATH_TO_BRANCH_SEP=$'\ue602'

BRANCH_DELMITER='2@'

DEFAULT_BG="%{${bg[blue]}%}"
DEFAULT_FG="%{${fg[white]}%}"

WARNING_BLOCK="%{${bg[yellow]}${fg[black]}%}"
ERROR_BLOCK="%{${reset_color}${fg[red]}%}"
DEFAULT_PATH_COLOUR="%{${bg[blue]}${fg[black]}%}"

TOPIC_BLOCK="%{${bg[yellow]}${fg[grey]}%}"

build_bg_and_failed_prompt() {
    local num_jobs pr
    if [ "$1" != "0" ]
    then
        echo -n "${ERROR_BLOCK}${FAILED_PROCESS_ICON}%{${reset_color}%}"
    fi
    num_jobs=$(jobs | wc -l)
    if [ ${num_jobs} -ne 0 ]
    then
        echo -n "${WARNING_BLOCK}${num_jobs}${BG_PROCESS_ICON}%{${reset_color}%}"
    fi
}

build_path_prompt() {
    local path git_dir
    path=$(echo $PWD | /usr/bin/sed -E "s/\/home\/$USER/~/g")

    if [ -n "$1" ]
    then
        git_dir=$(echo $1 | /usr/bin/sed -E "s/1@([^\.]*)\.*1@.*$/\1/g" | /usr/bin/sed -E "s/(.*)\/$/\1/g")
        path=$(echo ${path} | /usr/bin/sed -E "s#${git_dir}#%{$fg[white]%}${git_dir}#g")
    fi

    echo -n "${DEFAULT_PATH_COLOUR}${path}"
}

find_vcs_type() {
    case "$1" in
        git)
            echo "\uf1d3"
            ;;
        *)
            echo ""
            ;;
    esac
}

find_vcs_branch_fg_colour() {
    echo -n "%{${reset_color}%}"
    case "$1" in
        master)
        echo -n "%{${fg[red]}%}"
        ;;
        topic/*)
        echo -n "%{${fg[green]}%}"
        ;;
    esac
}

get_git_status_character() {
    case $1 in
        ADD)
            echo 'A'
            ;;
        MOD)
            echo 'M'
            ;;
        DEL)
            echo 'D'
            ;;
        REN)
            echo 'R'
            ;;
        UNK)
            echo '\\?'
            ;;
          *)
            echo '~'
            ;;
    esac
}

get_status_character() {
    case $1 in
        ADD)
            echo '\uf457'
            ;;
        MOD)
            echo '\uf8ec'
            ;;
        DEL)
            echo '\uf12d'
            ;;
        REN)
            echo '\uf45a'
            ;;
        UNK)
            echo '\uf29c'
            ;;
          *)
            echo ''
            ;;
    esac
}

display_count_num_and_symbol_for_git_status() {
    local status_line status_id count status_char
    if [ -n "$2" ] && [ -n "$1" ]
    then
        status_line=$1
        status_id=$(get_git_status_character $2)
        status_char=$(get_status_character $2)
        count="$(echo ${status_line}  | grep "${status_id}" | wc -l)"
    else
        count='0'
    fi
    if [ ${count} -ne 0 ]
    then
        echo " ${count} ${status_char}"
    fi
}

build_git_prompt() {
    local vcs_str vcs_branch vcs_type vcs_has_staged vcs_has_unstaged prompt_str local vcs_behind vcs_ahead vcs_tmp file_status
    vcs_str=$1

    if [ -n "${vcs_str}" ]
    then
        vcs_type=$(echo ${vcs_str} | sed -E "s/.*0@(.*)0@.*/\1/g")
        vcs_branch=$(echo ${vcs_str} | sed -E "s/.*2@(.*)2@.*/\1/g")
        vcs_action=$(echo ${vcs_str} | sed -E "s/.*3@(.*)3@.*/\1/g")

        if [ -n "${vcs_branch}" ]
        then
            prompt_str="$(find_vcs_branch_fg_colour ${vcs_branch})"
            vcs_tmp=$(git rev-list --left-right --count ${vcs_branch}...origin/${vcs_branch})
            vcs_ahead=$(echo ${vcs_tmp} | sed -E "s/^([[:digit:]]*).*/\1/g")
            vcs_behind=$(echo ${vcs_tmp} | sed -E "s/^[[:digit:]]*[[:space:]]*([[:digit:]]*)$/\1/g")
            file_status=$(git status -s | sed -E "s/[[:space:]]*([MADR\?])[[:space:]]*.*$/\1/g")
        fi


        if [ -n "${vcs_type}" ]
        then
            prompt_str="${prompt_str}$(find_vcs_type ${vcs_type})"
        fi

        if [ -n "${vcs_branch}" ]
        then
            prompt_str="${prompt_str} ${BRANCH_INDICATOR} ${vcs_branch}"
            if [ -n "${vcs_action}" ]
            then
                prompt_str="${prompt_str} (${vcs_action})"
            fi
            if [ 0 -ne ${vcs_behind} ]
            then
                prompt_str="${prompt_str} ${vcs_behind} \ufc08"
            fi
            if [ 0 -ne ${vcs_ahead} ]
            then
                prompt_str="${prompt_str} ${vcs_ahead} \ufbd0"
            fi
            local count_func
            count_func="display_count_num_and_symbol_for_${vcs_type}_status"
            for stat in 'ADD' 'MOD' 'DEL' 'REN' 'UNK'
            do
                prompt_str="${prompt_str}$(${count_func} ${file_status} ${stat})"
            done
        fi
    fi
    prompt_str="${prompt_str}"
    echo "${prompt_str}%{${reset_color}%}"
}

build_prompt_end() {
    echo "${PROMPT_END}%{${reset_color}%} "
}

build_prompt() {
    RETVAL=$?
    setup_vcs_info
    git_str="${vcs_info_msg_0_}"
    git_str=${(S%%)git_str}

    build_bg_and_failed_prompt ${RETVAL}
    build_path_prompt ${git_str}
    build_prompt_end
}

build_rprompt() {
    setup_vcs_info
    local git_str

    git_str="${vcs_info_msg_0_}"
    git_str=${(S%%)git_str}

    build_git_prompt ${git_str}
}

setup_vcs_info() {
    autoload -Uz vcs_info
    setopt prompt_subst
    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*' check-for-changes false
    zstyle ':vcs_info:*' get-revision true
    zstyle ':vcs_info:git:*' formats "0@%s0@ 1@%r/%S1@ 2@%b2@ 3@%a3@"
    vcs_info
}

PROMPT='$(build_prompt)'
RPROMPT='$(build_rprompt)'
